package fr.epsi.rennes.mspr.tpre814.analysemaintenabilite_p_stephen;

import fr.epsi.rennes.mspr.tpre814.analysemaintenabilite_p_stephen.dto.Car;
import fr.epsi.rennes.mspr.tpre814.analysemaintenabilite_p_stephen.dto.Customer;
import fr.epsi.rennes.mspr.tpre814.analysemaintenabilite_p_stephen.dto.Rental;
import fr.epsi.rennes.mspr.tpre814.analysemaintenabilite_p_stephen.enums.CarType;
import org.assertj.core.api.junit.jupiter.SoftAssertionsExtension;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(SoftAssertionsExtension.class)
class MainTests {

    @Test
    void defaultTest() {
        // Etant donné un client avec 2 locations
        Car car1 = new Car("Car 1", CarType.REGULAR);
        Car car2 = new Car("Car 2", CarType.NEW_MODEL);

        // Car 1 est louée 3 jours, Car 2 est louée 2 jours
        Rental rental1 = new Rental(car1, 3);
        Rental rental2 = new Rental(car2, 2);

        Customer customer = new Customer("John Doe");
        customer.addRental(rental1);
        customer.addRental(rental2);

        // Quand on demande la facture
        String actualInvoice = customer.invoice() + "";
        // Alors on obtient le résultat suivant
        assertEquals("""
                Rental Record for John Doe
                \tCar 1\t335.0
                \tCar 2\t390.0
                Amount owed is 725.0
                You earned 73 frequent renter points
                """, actualInvoice, "La facture est incorrecte pour le scénario par défaut");
    }

    @Test
    void testRegularCar() {
        Car car = new Car("Car 1", CarType.REGULAR);
        Rental rental = new Rental(car, 3);

        Customer customer = new Customer("John Doe");
        customer.addRental(rental);

        String actualInvoice = customer.invoice() + "";
        assertEquals("""
                Rental Record for John Doe
                \tCar 1\t335.0
                Amount owed is 335.0
                You earned 33 frequent renter points
                """, actualInvoice, "La facture est incorrecte pour une voiture classique");
    }
    @Test
    void testNewModelCar() {
        Car car = new Car("Car 2", CarType.NEW_MODEL);
        Rental rental = new Rental(car, 3);

        Customer customer = new Customer("John Doe");
        customer.addRental(rental);

        String actualInvoice = customer.invoice() + "";
        assertEquals("""
                Rental Record for John Doe
                \tCar 2\t540.0
                Amount owed is 540.0
                You earned 55 frequent renter points
                """, actualInvoice, "La facture est incorrecte pour une voiture de nouveau modèle");
    }

    @Test
    void testJsonInvoice() {
        Car car2 = new Car("Car 2", CarType.NEW_MODEL);

        Rental rental2 = new Rental(car2, 3);

        Customer customer = new Customer("John Doe");
        customer.addRental(rental2);

        String actualInvoice = customer.invoice().toJson();
        assertThat(actualInvoice).usingDefaultComparator().
                isEqualTo("{\"customer\": \"John Doe\"," +
                                "\"rentals\": [" +
                                "{\"title\": \"Car 2\",\"daysRented\": 3,\"amount\": 540.0}" +
                                "], " +
                                "\"totalAmount\": 540.0, " +
                                "\"frequentRenterPoints\": 55" +
                                "}"
                , "La facture JSON est incorrecte");
    }
}
