package fr.epsi.rennes.mspr.tpre814.analysemaintenabilite_p_stephen.metier;

import fr.epsi.rennes.mspr.tpre814.analysemaintenabilite_p_stephen.dto.Customer;
import fr.epsi.rennes.mspr.tpre814.analysemaintenabilite_p_stephen.dto.Rental;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Invoice extends InvoiceCalculator implements Formatted {

    private final Customer customer;
    private final List<Rental> rentals;

    public Invoice(Customer customer, List<Rental> rentals) {
        this.customer = customer;
        this.rentals = rentals;
    }


    /**
     * @return a string representation of the invoice in the following format:<br>
     * """<br>
     * Rental Record for customerName<br>
     * carTitle	priceOfRental<br>
     * ...<br>
     * carTitle	priceOfRental<br>
     * Amount owed is totalAmount<br>
     * You earned cumulatedPoints frequent renter points<br>
     * """
     */
    @Override
    public String toString() {
        Map<String, Double> lines = calculate(rentals);
        StringBuilder result = new StringBuilder("Rental Record for " + customer.getName() + "\n");

        // Add rental lines
        lines.forEach((carTitle, price) -> result
                .append("\t").append(carTitle)
                .append("\t").append("%.1f".formatted(price / 100))
                .append("\n"));

        // Total amount and frequent renter points
        result.append("Amount owed is ").append(String.format("%.1f", totalAmount / 100)).append("\n");
        result.append("You earned ").append(frequentRenterPoints).append(" frequent renter points\n");

        return result.toString();
    }

    public String toJson() {
        Map<String, Double> lines = calculate(rentals);
        StringBuilder result = new StringBuilder("{");
        result.append("\"customer\": \"").append(customer.getName()).append("\",");

        // Add rental lines
        List<String> rentalLines = new ArrayList<>();
        result.append("\"rentals\": [");
        lines.forEach((carTitle, price) -> {
            final int daysRented = rentals.stream()
                    .filter(rental -> rental.getCar().getTitle().equals(carTitle))
                    .findFirst().orElseThrow().getDaysRented();
            rentalLines.add("{\"title\": \"" + carTitle +
                    "\",\"daysRented\": " + daysRented +
                    ",\"amount\": " + price / 100 + "}");
        });
        result.append(String.join(",", rentalLines));
        result.append("], ");

        // Total amount and frequent renter points
        result.append("\"totalAmount\": ").append(totalAmount / 100).append(", ");
        result.append("\"frequentRenterPoints\": ").append(frequentRenterPoints);
        result.append("}");

        return result.toString();
    }
}
