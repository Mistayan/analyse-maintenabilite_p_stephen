package fr.epsi.rennes.mspr.tpre814.analysemaintenabilite_p_stephen.enums;

import lombok.Getter;

// Idéalement, il faudrait exporter cet Enum vers une base de données
// pour faciliter l'ajouter/modification/suppression EN LIVE des types de voitures disponibles,
// ainsi que les prix et limites, __sans avoir à recompiler__ le code source.
@Getter
public enum CarType {

    REGULAR(0, 5000, 9500, 10000, 5, 2),
    NEW_MODEL(1, 9000, 15000, 10000, 3, 2);

    public final int value;
    public final int basePrice;
    public final int pricePerDay;
    public final int discountAmount;
    public final int discountAfterNDays;
    public final int nbDiscountDays;

    CarType(int value, int basePrice, int pricePerDay, int discountAmount, int discountAfterNDays, int nbDiscountDays) {
        this.value = value;
        this.basePrice = basePrice;
        this.pricePerDay = pricePerDay;
        this.discountAmount = discountAmount;
        this.discountAfterNDays = discountAfterNDays;
        this.nbDiscountDays = nbDiscountDays;
    }

    public double calculateAmount(int daysRented) {
        double totalAmount = basePrice + daysRented * pricePerDay;
        if (daysRented > discountAfterNDays) {
            totalAmount -= (daysRented - nbDiscountDays) * discountAmount;
        }
        return totalAmount;
    }
}