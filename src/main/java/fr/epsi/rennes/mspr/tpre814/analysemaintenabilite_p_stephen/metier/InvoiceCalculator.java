package fr.epsi.rennes.mspr.tpre814.analysemaintenabilite_p_stephen.metier;

import fr.epsi.rennes.mspr.tpre814.analysemaintenabilite_p_stephen.dto.Rental;
import fr.epsi.rennes.mspr.tpre814.analysemaintenabilite_p_stephen.enums.CarType;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Calculate the total amount and the frequent renter points for the invoice
 */
public abstract class InvoiceCalculator {

    double totalAmount = 0.0;
    int frequentRenterPoints = 0;

    private static double getRentalAmount(Rental rental) {
        CarType carType = CarType.values()[rental.getCar().getPriceCode()];
        return carType.calculateAmount(rental.getDaysRented());
    }

    /**
     * 10% of total amount.
     * Exemple: 10% of 395.5 = 40
     *
     * @param rental la location
     * @return the number of points earned for this rental
     */
    private static int addRentalPoints(Rental rental, int frequentRenterPoints) {
        double amount = getRentalAmount(rental);
        int points = (int) (amount * 0.001);

        // On garde la méthode, étant donné que l'exercice stipule :
        // "... points de fidélité soient **également** calculés en fonction du montant total ..."
        // Ne stipulant pas les cas en fonction du type de voiture, je préfère garder cette méthode.
        // >>> Il est plus facile d'enlever que d'ajouter.
        if (isBonusApplicable(rental)) {
            points += 1;
        }

        return frequentRenterPoints + points;
    }

    private static boolean isBonusApplicable(Rental rental) {
        return CarType.values()[rental.getCar().getPriceCode()] == CarType.NEW_MODEL && rental.getDaysRented() > 1;
    }

    /**
     * Calculate the total amount and the frequent renter points for the invoice
     *
     * @return a map of `car title` and their corresponding `rental amount`
     */
    Map<String, Double> calculate(List<Rental> rentals) {
        totalAmount = 0.0;
        frequentRenterPoints = 0;

        Map<String, Double> eachLineAmount = new HashMap<>(rentals.size());
        for (Rental rental : rentals) {
            double thisAmount = getRentalAmount(rental);
            frequentRenterPoints = addRentalPoints(rental, frequentRenterPoints);
            totalAmount += thisAmount;
            eachLineAmount.put(rental.getCar().getTitle(), thisAmount);
        }
        return eachLineAmount;
    }

}
