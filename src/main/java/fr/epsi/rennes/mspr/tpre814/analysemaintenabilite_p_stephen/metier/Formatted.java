package fr.epsi.rennes.mspr.tpre814.analysemaintenabilite_p_stephen.metier;

public interface Formatted {
    String toString();
    String toJson();
}
