package fr.epsi.rennes.mspr.tpre814.analysemaintenabilite_p_stephen;

import fr.epsi.rennes.mspr.tpre814.analysemaintenabilite_p_stephen.dto.Car;
import fr.epsi.rennes.mspr.tpre814.analysemaintenabilite_p_stephen.dto.Customer;
import fr.epsi.rennes.mspr.tpre814.analysemaintenabilite_p_stephen.dto.Rental;
import fr.epsi.rennes.mspr.tpre814.analysemaintenabilite_p_stephen.enums.CarType;

public class Main {
    public static void main(String[] args) {
        Car car1 = new Car("Car 1", CarType.REGULAR);
        Car car2 = new Car("Car 2", CarType.NEW_MODEL);

        Rental rental1 = new Rental(car1, 3);
        Rental rental2 = new Rental(car2, 2);

        Customer customer = new Customer("John Doe");
        customer.addRental(rental1);
        customer.addRental(rental2);

        System.out.println(customer.invoice().toString());
        System.out.println(customer.invoice().toJson());
    }
}
