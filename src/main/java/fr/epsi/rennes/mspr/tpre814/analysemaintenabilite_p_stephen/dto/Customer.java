package fr.epsi.rennes.mspr.tpre814.analysemaintenabilite_p_stephen.dto;

import fr.epsi.rennes.mspr.tpre814.analysemaintenabilite_p_stephen.metier.Invoice;

import java.util.ArrayList;
import java.util.List;


public class Customer {
    private final String name;
    private final List<Rental> rentals = new ArrayList<>();

    public Customer(String name) {
        this.name = name;
    }

    public void addRental(Rental rental) {
        rentals.add(rental);
    }

    public String getName() {
        return name;
    }

    public Invoice invoice() {
        return new Invoice(this, this.rentals);
    }
}
