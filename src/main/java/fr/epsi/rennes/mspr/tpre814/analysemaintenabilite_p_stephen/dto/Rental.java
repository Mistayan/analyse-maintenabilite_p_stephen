package fr.epsi.rennes.mspr.tpre814.analysemaintenabilite_p_stephen.dto;

public record Rental(Car car, int daysRented) {
    public Car getCar() {
        return car;
    }

    public int getDaysRented() {
        return daysRented;
    }
}
