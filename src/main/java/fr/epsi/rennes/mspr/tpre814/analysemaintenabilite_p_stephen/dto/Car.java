package fr.epsi.rennes.mspr.tpre814.analysemaintenabilite_p_stephen.dto;

import fr.epsi.rennes.mspr.tpre814.analysemaintenabilite_p_stephen.enums.CarType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class Car {
    private String title;
    private CarType type;

    // -> permet de ne pas modifier le contrat de l'interface
    @Deprecated(forRemoval = false, since = "Kept for backward compatibility")
    public Car(String title, int priceCode) {
        this.title = title;
        this.type = CarType.values()[priceCode];
    }

    public int getPriceCode() {
        return type.getValue();
    }


}
