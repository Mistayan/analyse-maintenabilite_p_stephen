# analyse-maintenabilite_p_stephen


## Table des matières

[TOC]


____
## Exercice 1 : Questions sur les acquis notions vues en cours

1. Quelles sont les principales sources de complexité dans un système logiciel (sources d’un
programme) ?
```
Les principales sources de complexité dans un système logiciel sont les suivantes :
- Mutabilité : la complexité liée à la gestion des états et des changements d'états.
- Duplications : la complexité liée à la répétition de code.
- Opacité : la complexité liée à la compréhension du code.
- Rigidité : la complexité liée à la difficulté de modification du code.
- Fragilité : la complexité liée à la difficulté de correction de bugs.
```

2. Quel(s) avantage(s) procure le fait de programmer vers une interface et non vers une
implémentation ? Vous pouvez illustrer votre réponse avec un code source minimal et/ou avec un
diagramme.
```
Une interface nous permet de définir un contrat entre les différentes parties de notre code.
Cela permet de définir des règles et des contraintes que les classes qui implémentent cette interface doivent respecter. (IN/OUT)
Cela permet de réduire la complexité du code en définissant des règles claires et en séparant les différentes parties du code.
Cela permet également de faciliter la maintenance du code en permettant de remplacer facilement une implémentation par une autre sans modifier le code existant:
Ou encore à définir des comportements par défaut pour des classes qui implémentent cette interface.
```

```java
public interface Animal {
    default void eat() { // Comportement par défaut pour toutes les classes héritant de cette interface
        System.out.printf("%s is eating%n", this.getClass().getSimpleName());
    }

    // Méthodes abstraites
    void sleep();  

    void move();
}

public abstract class Mammal implements Animal {
    int numberOfLegs = 4;

    @Override
    public void move() {
        System.out.printf("%s is moving on his %d legs%n", this.getClass().getSimpleName(), numberOfLegs);
    }
}

public class Dog implements BaseAnimal {
    @Override
    public void sleep() {
        System.out.println("Dog is sleeping on the carpet");
    }
}

public class Cat implements BaseAnimal {
    String[] preferedSpots = {"sofa", "bed", "chair"};
    @Override
    public void sleep() {
        String spot = preferedSpots[(int) (Math.random() * preferedSpots.length)];
        System.out.printf("Cat is sleeping on the %s%n", spot);
    }
}

public class Main {
    public static void main(String[] args) {
        Animal dog = new Dog();
        Animal cat = new Cat();

        dog.eat();
        dog.move();
        dog.sleep();

        cat.eat();
        cat.move();
        cat.sleep();
    }
}
```

3. Eric Steven Raymond, figure du mouvement open-source et hacker notoire, a récemment écrit sur
le développement d’un système : “First make it run, next make it correct, and only after that worry
about making it fast.” que l’on peut traduire par “D’abord, faites en sorte que ça fonctionne,
ensuite assurez-vous que ce soit correct, et seulement après, préoccupez-vous de le rendre rapide.”.
Comment comprenez-vous chaque étape de cette heuristique ? Comment comprenez-vous cette
heuristique dans son ensemble ?

```text
Eric Steven Raymond nous rappelle l'importance de la priorisation des tâches lors du développement d'un système : Il est inutile d'avoir un système rapide s'il ne fait rien de correct.
```

4. Nous avons vu en cours une technique pour lutter contre la déstructuration du code au cours du
temps, le refactoring. Quelle méthode ou approche est recommandée pour mener à bien un travail
de refactoring ?

```text
Une méthode fortement recommandée pour mener à bien un travail de refactoring : "Red Green Refactor".
- Red : Ecrire un test qui échoue
- Green : Ecrire le code qui fait passer le test
- Refactor : Améliorer le code sans changer son comportement (les résultats des tests doivent rester les mêmes)
             Il arrive parfois que le refactoring nécessite de modifier les tests, surtout si l'on sépare des responsabilités en plusieurs classes et services.
             Dans ce cas, il est important de s'assurer que les résultats des tests restent valides !
```

____

## Exercice 2 : Analyse de code

Technologies utilisées:
- Java 21+
- JUnit 5.10.2+
- Gradle 8.8+
- Docker (pour le déploiement)


### 2.1. Comment lancer l'application ?

Si vous souhaitez simplement lancer l'application en local :
```shell
 gradle bootRun
```

### 2.2. Comment lancer les tests ?

```shell
gradle test
```

Vous ne souhaitez pas installer Java, gradle ? Pas de soucis, vous pouvez utiliser Docker pour exécuter les tests et lancer l'application.

Pour exécuter les tests avec une image gradle :
```shell
docker run -it --rm -v "$(pwd):/app" -w /app gradle:8.8.0-jdk21-alpine gradle test bootRun ; echo /app/build/reports/tests/test/index.html
```

Si vous souhaitez supprimer l'image gradle après l'exécution des tests :
```shell
docker rmi gradle:8.8.0-jdk21-alpine
```

### 2.3. Commentaires

Désolé d'avoir converti en Java.